export default class Background {
  static defineBackgroundPositions(backgroundSizes, backgroundSize) {
    // Used to calculate the value of the background-position CSS property.
    let newBackgroundPositions = '';
    for (let index = 0; index < backgroundSizes.length; index += 1) {
      const newBackgroundPosition = parseInt(Math.random() * backgroundSize, 10);
      if (index % 2 === 0) {
        newBackgroundPositions += `${newBackgroundPosition}px `;
      } else if (index + 1 === backgroundSizes.length) {
        newBackgroundPositions += `${newBackgroundPosition}px`;
      } else {
        newBackgroundPositions += `${newBackgroundPosition}px,`;
      }
    }
    return newBackgroundPositions;
  }

  static animation() {
    // Randomization of background positions of the stars shown in the body at each page load.
    const backgroundSizes = $('body').css('background-size').replace(/px/g, '').replace(/,/g, '')
      .split(' ');
    const backgroundSize = backgroundSizes[0];
    const newBackgroundPositionsMoving = [];
    // newBackgroundPositionsMoving is used if the easter egg is activated.
    for (let index = 0; index < 4; index += 1) {
      newBackgroundPositionsMoving.push(
        this.defineBackgroundPositions(backgroundSizes, backgroundSize));
    }
    const newBackgroundPositions = this.defineBackgroundPositions(backgroundSizes, backgroundSize);
    $('body').css('background-position', newBackgroundPositions);
    // The animation doesn't work on Google Chrome if background positions aren't declared at each
    // CSS keyframe, even if its value doesn't change. Here, keyframes are updated.
    const styles = document.styleSheets;
    for (let i = 0; i < styles.length; i += 1) {
      for (let j = 0; j < styles[i].cssRules.length; j += 1) {
        if ((styles[i].cssRules[j].name === 'stars') || (styles[i].cssRules[j].name === 'starsMoving')) {
          for (let k = 0; k < styles[i].cssRules[j].cssRules.length; k += 1) {
            if (styles[i].cssRules[j].name === 'starsMoving') {
              styles[i].cssRules[j].cssRules[k].style.backgroundPosition = newBackgroundPositionsMoving[k];
            } else {
              styles[i].cssRules[j].cssRules[k].style.backgroundPosition = newBackgroundPositions;
            }
          }
        }
      }
    }
    // Background repeat is "disabled" at page load to avoid positioning glitches.
    $('body').css('background-repeat', 'repeat');
  }
}

import prestashop from 'prestashop';

export default class Easter {
  static cleanUpSpecialChars(str) {
    // Replaces accented characters by equivalent letters.
    // Used to get the correct name of each animated letter file.
    return str
      .replace(/[àâä]/g, 'a')
      .replace(/[éèêë]/g, 'e')
      .replace(/[îï]/g, 'i')
      .replace(/[ôö]/g, 'o')
      .replace(/[ùûü]/g, 'u')
      .replace('ç', 'c')
      .replace('?', 'questionmark')
      .replace('!', 'exclamationmark')
      .replace(/[^a-z0-9]/gi, '');
  }

  static changeBanner(element, currentLetter, currentString) {
    // All letters from the banners are replaced by animated GIF letters.
    const nextLetter = currentLetter + 1;
    let newString = null;
    if (element.text().charAt(currentLetter) !== ' ') {
      newString = `${currentString}<img src="${prestashop.urls.base_url}themes/99in1/assets/img/letters/${Easter.cleanUpSpecialChars(element.text().charAt(currentLetter).toLowerCase())}.gif" alt="${element.text().charAt(currentLetter)}" width="50">`;
    } else {
      newString = `${currentString} `;
    }
    if (nextLetter !== element.text().length) {
      Easter.changeBanner(element, nextLetter, newString);
    } else {
      // When all letters have been changed, easter effects are applied on all the website.
      // If NNIO Random 8 bit Music is installed, the music is launched with its own easter eggs.
      element.html(newString);
      $('body').addClass('easter');
      if ($('#nnio-random8bitmusic').length !== 0) {
        easterEgg('audac', 'e');
        easterEgg(' qvgd', 'm');
        if (Tone.Transport.state !== 'started') {
          $('#nnio-random8bitmusic').click();
        }
      }
    }
  }

  static test() {
    // After clicking seven times on the banner, the easter egg launches.
    $(this).data('easter', $(this).data('easter') + 1);
    if ($(this).data('easter') === 7) {
      $('.easter').each(function () {
        Easter.changeBanner($(this), 0, '');
      });
    }
  }
}

export default class Logo {
  static animation(letter) {
    // Logo animation, inspired by the logo from the Soccer game released on NES.
    // Letters from the logo are highlighted following a cycle.
    let nextLetter = letter + 1;
    if ($('.animated-logo').text().charAt(letter) !== ' ') {
      $('.animated-logo').html(`${$('.animated-logo').text().slice(0, letter)}<span class="highlight">${$('.logo').text().charAt(letter)}</span>${$('.animated-logo').text().slice(letter + 1, $('.animated-logo').text().length)}`);
      if (nextLetter === $('.animated-logo').text().length) {
        nextLetter = 0;
      }
      setTimeout(() => {
        Logo.animation(nextLetter);
      }, 60);
    } else {
      this.animation(nextLetter);
    }
  }
}

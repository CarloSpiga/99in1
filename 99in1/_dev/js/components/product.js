export default class Product {
  static thumbnails(event) {
    // On the product page, changes the product image shown by clicking on its thumbnails.
    event.preventDefault();
    $(this).closest('.product-gallery').find('.product-thumbnails-parent').attr('src', $(this).data('image-large'));
  }
}

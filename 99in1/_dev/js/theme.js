/**
 * This is the entry point for specific javascript of theme
 */

import 'bootstrap';

import Background from './components/background';
import Logo from './components/logo';
import Product from './components/product';
import Easter from './components/easter';

$(() => {
  Background.animation();
  Logo.animation(0);
  $('.product-images .product-thumbnail').click(Product.thumbnails);
  $('.easter').click(Easter.test);
});

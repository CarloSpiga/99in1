/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

const path = require('path');
// Separate CSS from JS and minify it.
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// CSS sprites generation.
const SpritesmithPlugin = require('webpack-spritesmith');

const config = {
  mode: 'production',
  entry: {
    main: [
      './js/theme.js',
      './css/theme.scss',
    ],
  },
  output: {
    path: path.resolve(__dirname, '../assets/js'),
    filename: 'theme.js',
  },
  module: {
    rules: [
      {
        test: /\.js/,
        loader: 'babel-loader',
      },
      {
        test: /\.scss$/,
        use: [{
          loader: MiniCssExtractPlugin.loader,
        },
        {
          loader: 'css-loader',
          options: {
            sourceMap: true,
          },
        },
        'sass-loader',
        ],
      },
      {
        test: /.(woff(2)?)/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '../fonts/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '../img/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  externals: {
    prestashop: 'prestashop',
    $: '$',
    jquery: 'jQuery',
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: true,
        test: /\.js($|\?)/i,
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '../css/theme.css',
      chunkFilename: '../css/[id].css',
    }),
    new SpritesmithPlugin({
      src: {
        cwd: path.resolve(__dirname, './img/sprites'),
        glob: '*.png',
      },
      target: {
        image: path.resolve(__dirname, './dist/sprites.png'),
        css: path.resolve(__dirname, './dist/sprites.scss'),
      },
      apiOptions: {
        cssImageRef: '../dist/sprites.png',
      },
    }),
  ],
};

module.exports = config;

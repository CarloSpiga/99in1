<?php

class Adcwb_Customerscounter extends Module
{
    public function __construct()
    {
        $this->name = 'adcwb_customerscounter';
        $this->tab = 'front_office_features';
        $this->version = '0.0.1';
        $this->author = 'Audaceweb';
        $this->need_instance = 0;

        parent::__construct();

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $this->displayName = $this->l('Audaceweb Customers Counter');
        $this->description = $this->l('Shows the amount of customers on the theme.');
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('displayFooterAfter');
    }


    public function uninstall()
    {
        return parent::uninstall()
            && $this->unregisterHook('displayFooterAfter');
    }

    public function hookDisplayFooterAfter()
    {
        $customers = Db::getInstance()->getRow('SELECT COUNT(*) AS count FROM '._DB_PREFIX_.'customer');
        $this->context->smarty->assign('customers', $customers['count']);
        return $this->display(__FILE__, 'footer-after.tpl');
    }

    public function getContent()
    {
    }
}

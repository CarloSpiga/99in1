# Audaceweb Remove Default Assets

Remove logo and banner images from templates without deleting them thanks to overrides.
This way, you can show either the logo or their alternative text (that can be styled with CSS) only by enabling/disabling the module.

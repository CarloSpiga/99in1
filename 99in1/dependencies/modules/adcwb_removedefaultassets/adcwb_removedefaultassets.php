<?php

class Adcwb_Removedefaultassets extends Module
{
    public function __construct()
    {
        $this->name = 'adcwb_removedefaultassets';
        $this->tab = 'front_office_features';
        $this->version = '0.0.1';
        $this->author = 'Audaceweb';
        $this->need_instance = 0;

        parent::__construct();

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $this->displayName = $this->l('Audaceweb Remove Default Assets');
        $this->description = $this->l('Remove logo and banner images from templates.');
    }

    public function getContent()
    {
    }

    public function install()
    {
        return parent::install() &&
            $this->installFixture();
    }

    protected function installFixture()
    {
        if (Configuration::get('BANNER_DESC', $this->context->language->id) == '') {
            $values['BANNER_DESC'][$this->context->language->id] = 'PS_Banner Description';
            Configuration::updateValue('BANNER_DESC', $values['BANNER_DESC']);
        }
        return true;
    }
}

<?php
class FrontController extends FrontControllerCore
{
    public function getTemplateVarShop()
    {
        $shop = parent::getTemplateVarShop();
        $shop['logo'] = null;
        return $shop;
    }
}

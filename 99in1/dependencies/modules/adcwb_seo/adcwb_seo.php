<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

require_once _PS_MODULE_DIR_.'adcwb_seo/classes/AdcwbSeo.php';

class Adcwb_Seo extends Module implements WidgetInterface
{
    private $templateFile;

    public function __construct()
    {
        $this->name = 'adcwb_seo';
        $this->author = 'Audaceweb';
        $this->version = '0.0.1';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        Shop::addTableAssociation('adcwb_seo', array('type' => 'shop'));

        $this->displayName = $this->trans('Audaceweb SEO', array(), 'Modules.Adcwbseo.Admin');
        $this->description = $this->trans('Integrates custom keywords for SEO anywhere in your store front', array(), 'Modules.Adcwbseo.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.4.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:adcwb_seo/adcwb_seo.tpl';
    }

    public function install()
    {
        return $this->runInstallSteps()
            && $this->installFixtures();
    }

    public function runInstallSteps()
    {
        return parent::install()
            && $this->installDB()
            && $this->registerHook('displayFooterAfter')
            && $this->registerHook('actionShopDataDuplication');
    }

    public function uninstall()
    {
        return parent::uninstall() && $this->uninstallDB();
    }

    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute(
            '
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'adcwb_seo` (
                `id_adcwb_seo` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                PRIMARY KEY (`id_adcwb_seo`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute(
            '
                CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'adcwb_seo_shop` (
                `id_adcwb_seo` INT(10) UNSIGNED NOT NULL,
                `id_shop` INT(10) UNSIGNED NOT NULL,
                PRIMARY KEY (`id_adcwb_seo`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute(
            '
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'adcwb_seo_lang` (
                `id_adcwb_seo` INT UNSIGNED NOT NULL,
                `id_shop` INT(10) UNSIGNED NOT NULL,
                `id_lang` INT(10) UNSIGNED NOT NULL ,
                `text_adcwb_seo` text NOT NULL,
                PRIMARY KEY (`id_adcwb_seo`, `id_lang`, `id_shop`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        return $return;
    }

    public function uninstallDB($drop_table = true)
    {
        $ret = true;
        if ($drop_table) {
            $ret &= Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'adcwb_seo`')
                && Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'adcwb_seo_shop`')
                && Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'adcwb_seo_lang`');
        }

        return $ret;
    }

    public function getContent()
    {
        $output = '';

        if (Tools::isSubmit('saveadcwb_seo')) {
            if (!Tools::getValue('text_adcwb_seo_'.(int)Configuration::get('PS_LANG_DEFAULT'), false)) {
                $output = $this->displayError($this->trans('Please fill out all fields.', array(), 'Admin.Notifications.Error')) . $this->renderForm();
            } else {
                $update = $this->processSaveAdcwbSeo();

                if (!$update) {
                    $output = '<div class="alert alert-danger conf error">'
                        .$this->trans('An error occurred on saving.', array(), 'Admin.Notifications.Error')
                        .'</div>';
                }

                $this->_clearCache($this->templateFile);
            }
        }

        return $output.$this->renderForm();
    }

    public function processSaveAdcwbSeo()
    {
        $shops = Tools::getValue('checkBoxShopAsso_configuration', array($this->context->shop->id));
        $text = array();
        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $text[$lang['id_lang']] = Tools::getValue('text_adcwb_seo_'.$lang['id_lang']);
        }

        $saved = true;
        foreach ($shops as $shop) {
            Shop::setContext(Shop::CONTEXT_SHOP, $shop);
            $info = new AdcwbSeo(Tools::getValue('id_adcwb_seo', 1));
            $info->text_adcwb_seo = $text;
            $saved &= $info->save();
        }

        return $saved;
    }

    protected function renderForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $fields_form = array(
            'legend' => array(
                'title' => $this->trans('Audaceweb SEO', array(), 'Modules.Adcwbseo.Admin'),
            ),
            'input' => array(
                'id_adcwb_seo' => array(
                    'type' => 'hidden',
                    'name' => 'id_adcwb_seo'
                ),
                'content' => array(
                    'type' => 'text',
                    'label' => $this->trans('Keywords', array(), 'Modules.Adcwbseo.Admin'),
                    'lang' => true,
                    'name' => 'text_adcwb_seo',
                ),
            ),
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Admin.Actions'),
            ),
            'buttons' => array(
                array(
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                    'title' => $this->trans('Back to list', array(), 'Admin.Actions'),
                    'icon' => 'process-icon-back'
                )
            )
        );

        if (Shop::isFeatureActive() && Tools::getValue('id_adcwb_seo') == false) {
            $fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->trans('Shop association', array(), 'Admin.Global'),
                'name' => 'checkBoxShopAsso_theme'
            );
        }


        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'adcwb_seo';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'saveadcwb_seo';

        $helper->fields_value = $this->getFormValues();

        return $helper->generateForm(array(array('form' => $fields_form)));
    }

    public function getFormValues()
    {
        $fields_value = array();
        $idShop = $this->context->shop->id;
        $idInfo = AdcwbSeo::getAdcwbSeoIdByShop($idShop);

        Shop::setContext(Shop::CONTEXT_SHOP, $idShop);
        $info = new AdcwbSeo((int)$idInfo);

        $fields_value['text_adcwb_seo'] = $info->text_adcwb_seo;
        $fields_value['id_adcwb_seo'] = $idInfo;

        return $fields_value;
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('adcwb_seo'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('adcwb_seo'));
    }
    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'adcwb_seo_lang`
            WHERE `id_lang` = '.(int)$this->context->language->id.' AND  `id_shop` = '.(int)$this->context->shop->id;

        return array(
            'adcwb_seo' => Db::getInstance()->getRow($sql),
        );
    }

    public function installFixtures()
    {
        $return = true;
        $tabTexts = array(
            array(
                'text_adcwb_seo' => 'mot, clé, mot-clé'
            ),
        );

        $shopsIds = Shop::getShops(true, null, true);
        $languages = Language::getLanguages(false);
        $text = array();

        foreach ($tabTexts as $tab) {
            $info = new AdcwbSeo();
            foreach ($languages as $lang) {
                $text[$lang['id_lang']] = $tab['text_adcwb_seo'];
            }
            $info->text_adcwb_seo = $text;
            $return &= $info->add();
        }

        if ($return && sizeof($shopsIds) > 1) {
            foreach ($shopsIds as $idShop) {
                Shop::setContext(Shop::CONTEXT_SHOP, $idShop);
                $info->text_adcwb_seo = $text;
                $return &= $info->save();
            }
        }

        return $return;
    }

    /**
     * Add AdcwbSeo when adding a new Shop
     *
     * @param array $params
     */
    public function hookActionShopDataDuplication($params)
    {
        if ($infoId = AdcwbSeo::getAdcwbSeoIdByShop($params['old_id_shop'])) {
            Shop::setContext(Shop::CONTEXT_SHOP, $params['old_id_shop']);
            $oldInfo = new AdcwbSeo($infoId);

            Shop::setContext(Shop::CONTEXT_SHOP, $params['new_id_shop']);
            $newInfo = new AdcwbSeo($infoId, null, $params['new_id_shop']);
            $newInfo->text_adcwb_seo = $oldInfo->text_adcwb_seo;

            $newInfo->save();
        }
    }
}

<?php

class AdcwbSeo extends ObjectModel
{
    /** @var int $id_adcwb_seo - the ID of AdcwbSEO */
    public $id_adcwb_seo;

    /** @var String $text_adcwb_seo - AdcwbSEO values */
    public $text_adcwb_seo;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'adcwb_seo',
        'primary' => 'id_adcwb_seo',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => array(
            'id_adcwb_seo' =>			array('type' => self::TYPE_NOTHING, 'validate' => 'isUnsignedId'),
            'text_adcwb_seo' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'required' => true),
        )
    );

    /**
     * Return the AdcwbSEO ID By shop ID
     *
     * @param int $shopId
     * @return bool|int
     */
    public static function getAdcwbSeoIdByShop($shopId)
    {
        $sql = 'SELECT i.`id_adcwb_seo` FROM `' . _DB_PREFIX_ . 'adcwb_seo` i
		LEFT JOIN `' . _DB_PREFIX_ . 'adcwb_seo_shop` ish ON ish.`id_adcwb_seo` = i.`id_adcwb_seo`
		WHERE ish.`id_shop` = ' . (int)$shopId;

        if ($result = Db::getInstance()->executeS($sql)) {
            return (int) reset($result)['id_adcwb_seo'];
        }

        return false;
    }
}

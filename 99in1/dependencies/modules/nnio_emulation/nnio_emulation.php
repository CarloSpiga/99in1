<?php

class Nnio_Emulation extends Module
{
    public function __construct()
    {
        $this->name = 'nnio_emulation';
        $this->tab = 'front_office_features';
        $this->version = '0.0.1';
        $this->author = 'Audaceweb';
        $this->need_instance = 0;

        parent::__construct();

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $this->displayName = $this->l('99in1 Emulation');
        $this->description = $this->l('Read game ROMs into canvas with different libraries.');

        $this->js_path = 'modules/'.$this->name.'/views/js/';
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('Header')
            && $this->registerHook('displayFooterProduct');
    }


    public function uninstall()
    {
        return parent::uninstall()
            && $this->unregisterHook('Header')
            && $this->unregisterHook('displayFooterProduct');
    }

    public function hookDisplayFooterProduct()
    {
        return $this->display(__FILE__, 'footer-product.tpl');
    }

    public function hookHeader()
    {
        // Emulation is enabled only on product pages that have an attachment which is a .nes file.
        // This file will be played by the emulator.
        if (isset($this->context->controller->php_self) && $this->context->controller->php_self == 'product') {
            $product = new Product((int)Tools::getValue('id_product'));
            if ($product->getAttachments(Context::getContext()->language->id)) {
                foreach ($product->getAttachments(Context::getContext()->language->id) as $attachment) {
                    $fileNameParts = explode('.', $attachment['file_name']);
                    $extensions = ['nes'];
                    if (in_array($fileNameParts[count($fileNameParts)-1], $extensions)) {
                        // JSNES emulator
                        $this->context->controller->registerJavascript('nnio-emulation-lib', $this->js_path.'jsnes.min.js');
                        // Keyboard mapping for JSNES emulator.
                        $this->context->controller->registerJavascript('nnio-emulation-code', $this->js_path.'nes-embed.js');
                        // ROM loading in the emulator.
                        $this->context->controller->registerJavascript('nnio-emulation-init', $this->js_path.'scripts.js');
                        break;
                    }
                }
            }
        }
    }

    public function getContent()
    {
    }
}

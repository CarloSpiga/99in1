{if $product.attachments}
  {foreach from=$product.attachments item=attachment}
    {assign var="array_file_name" value="."|explode:$attachment.file_name}
    {if $array_file_name.1 == 'nes'}
      <div class="row nnio-emulation">
          <div class="col-auto text-right">
              <canvas id="nes-canvas" width="256" height="240">{l s="Unable to load the game." mod="nnio_emulation"}</canvas>
          </div>
          <div class="col-auto">
              <ul>
                  <li>{l s="UP" mod="nnio_emulation"} : <kbd>z</kbd></li>
                  <li>{l s="LEFT" mod="nnio_emulation"} : <kbd>q</kbd></li>
                  <li>{l s="DOWN" mod="nnio_emulation"} : <kbd>s</kbd></li>
                  <li>{l s="RIGHT" mod="nnio_emulation"} : <kbd>d</kbd></li>
                  <li>{l s="A" mod="nnio_emulation"} : <kbd>p</kbd></li>
                  <li>{l s="B" mod="nnio_emulation"} : <kbd>o</kbd></li>
                  <li>{l s="SELECT" mod="nnio_emulation"} : <kbd>t</kbd></li>
                  <li>{l s="START" mod="nnio_emulation"} : <kbd>y</kbd></li>
              </ul>
          </div>
      </div>
      <script>
        var nnio_rom_id = "{$attachment.id_attachment}";
        var nnio_rom = "{url entity='attachment' params=['id_attachment' => $attachment.id_attachment]}";
      </script>
      {break}
    {/if}
  {/foreach}
{/if}

# 99in1 Random 8-bit Music

Adds a player that plays random 8-bit music on each page.
Based on the work of [Greg Hovanesyan](https://codepen.io/gregh/post/recreating-legendary-8-bit-games-music-with-web-audio-api).

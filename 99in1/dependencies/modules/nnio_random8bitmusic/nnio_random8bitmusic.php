<?php

class Nnio_Random8bitmusic extends Module
{
    public function __construct()
    {
        $this->name = 'nnio_random8bitmusic';
        $this->tab = 'front_office_features';
        $this->version = '0.0.1';
        $this->author = 'Audaceweb';
        $this->need_instance = 0;

        parent::__construct();

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $this->displayName = $this->l('99in1 Random 8-bit Music');
        $this->description = $this->l('Randomizes notes from famous video games musics.');

        $this->js_path = 'modules/'.$this->name.'/views/js/';
        $this->audio_path = 'modules/'.$this->name.'/views/audio/';
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('Header')
            && $this->registerHook('displayNav');
    }


    public function uninstall()
    {
        return parent::uninstall()
            && $this->unregisterHook('Header')
            && $this->unregisterHook('displayNav');
    }

    public function hookDisplayNav()
    {
        $this->context->smarty->assign('json_folder', $this->js_path.'notes/');
        $this->context->smarty->assign('audio_folder', $this->audio_path);
        return $this->display(__FILE__, 'nav.tpl');
    }

    public function hookHeader()
    {
        // Tone.js library to manipulate Web Audio https://tonejs.github.io/
        $this->context->controller->registerJavascript('nnio-random8bitmusic-lib', $this->js_path.'Tone.js');
        // This file has been edited from https://codepen.io/gregh/post/recreating-legendary-8-bit-games-music-with-web-audio-api
        // and converts JSON files to notes to be played by the Web Audio API.
        $this->context->controller->registerJavascript('nnio-random8bitmusic-code', $this->js_path.'app.js');
        // New file that gets the JSON files containing the notes and shuffles them.
        $this->context->controller->registerJavascript('nnio-random8bitmusic-init', $this->js_path.'scripts.js');
    }

    public function getContent()
    {
    }
}

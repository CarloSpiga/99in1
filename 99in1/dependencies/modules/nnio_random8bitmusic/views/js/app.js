// This file comes from https://codepen.io/gregh/post/recreating-legendary-8-bit-games-music-with-web-audio-api
var pulseOptions = {
  oscillator:{
  	type: "pulse"
  },
  envelope:{
    release: 0.07
  }
};

var triangleOptions = {
  oscillator:{
  	type: "triangle"
  },
  envelope:{
    release: 0.07
  }
};

var squareOptions = {
  oscillator:{
  	type: "square"
  },
  envelope:{
    release: 0.07
  }
};

var pulseSynth    = new Tone.Synth(pulseOptions).toMaster();
var squareSynth   = new Tone.Synth(squareOptions).toMaster();
var triangleSynth = new Tone.Synth(triangleOptions).toMaster();
var noiseSynth    = new Tone.NoiseSynth().toMaster();

var song = {};
var pulsePart    = new Tone.Part();
var squarePart   = new Tone.Part();
var trianglePart = new Tone.Part();
var noisePart    = new Tone.Part();

var button = document.getElementById('nnio-random8bitmusic');
button.firstChild.setAttribute('src', btnNoSoundSrc);

function setSong(newSong) {

  song = newSong;

  pulsePart.removeAll();
  squarePart.removeAll();
  trianglePart.removeAll();
  noisePart.removeAll();

  if(song.pulse != null) {
    pulsePart = new Tone.Part(function(time, note){
      pulseSynth.triggerAttackRelease(note.name, note.duration, time, note.velocity);
    }, song.pulse);
  }

  if(song.square != null) {
    squarePart = new Tone.Part(function(time, note){
      squareSynth.triggerAttackRelease(note.name, note.duration, time, note.velocity);
    }, song.square);
  }

  if(song.triangle != null) {
    trianglePart = new Tone.Part(function(time, note){
      triangleSynth.triggerAttackRelease(note.name, note.duration, time, note.velocity);
    }, song.triangle);
  }

  if(song.noise != null) {
    noisePart = new Tone.Part(function(time, note){
      noiseSynth.triggerAttackRelease(note.duration, time, note.velocity);
    }, song.noise);
  }

  button.disabled = false;

}

function start() {

  if(song.pulse != null)    pulsePart.start(0);
  if(song.square != null)   squarePart.start(0);
  if(song.triangle != null) trianglePart.start(0);
  if(song.noise != null)    noisePart.start(0);

}

function stop() {

  if(song.pulse != null)    pulsePart.stop(0);
  if(song.square != null)   squarePart.stop(0);
  if(song.triangle != null) trianglePart.stop(0);
  if(song.noise != null)    noisePart.stop(0);

}

button.addEventListener('click', function(){

  if(Tone.Transport.state == 'started') {
    Tone.Transport.stop();
    button.firstChild.setAttribute('src', btnNoSoundSrc);
    button.firstChild.setAttribute('alt', 'Restart');

    stop();

  } else {

    Tone.Transport.start('+0.1', 0);

    start();

    button.firstChild.setAttribute('src', btnSoundSrc);
    button.firstChild.setAttribute('alt', 'Stop');
  }
})

Tone.Transport.on('stop', function(){
  button.firstChild.setAttribute('src', btnNoSoundSrc);
  button.firstChild.setAttribute('alt', 'Restart');
})

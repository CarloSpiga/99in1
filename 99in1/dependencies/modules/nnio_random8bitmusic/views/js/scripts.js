function arrayShuffle(a) {
    // Shuffles an array.
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function shuffleNotes(notes) {
    // JSON files contains a note.name field which has a note as value. Notes are shuffled.
    if (notes.length != 0) {
        let namesNotes = []
        for (note of notes) {
            namesNotes.push(note.name);
        }
        let timeTotal = note.time + note.duration;
        namesNotes = arrayShuffle(namesNotes);
        for ([index, note] of notes.entries()) {
            note.name = namesNotes[index];
        }
        return [notes, timeTotal];
    } else {
        return [];
    }
}

function processAfterShuffle(bass1, bass2, synth1, synth2) {
    // JSON files are converted to Tone.js instruments.
    if (bass1 && bass2 && synth1 && synth2) {
        if (bass1.length != 0) {
            song.triangle = bass1[0];
            song.length = bass1[1];
        }
        if (bass2.length != 0) {
            song.noise = bass2[0];
            song.length = bass2[1];
        }
        if (synth1.length != 0) {
            song.pulse = synth1[0];
            song.length = synth1[1];
        }
        if (synth2.length != 0) {
            song.square = synth2[0];
            song.length = synth2[1];
        }
        setSong(song);
    }
}

function getRandomArbitrary(min, max) {
    // Returns a random number included between two values.
    return Math.random() * (max - min) + min;
}

function easterEggStart(egg, volume) {
    // Easter egg usics parameters.
    egg.loop = true;
    egg.volume.value = volume;
    egg.start();
}

function easterEgg(pressed, key) {
    // Reference to the two other projects of the Audaceweb agency :
    // https://www.carlospiga.fr/audaceweb/ : CCI project 1
    // https://www.carlospiga.fr/QVGDM/ : CCI project 2
    // Famous musics from these two projects are played with the notes from the modules.
    // Typing "audace" or "qvgdm" on the keyboard toggles them.
    let egg = null;
    pressed += key;
    if (pressed.length > 6) {
        pressed = pressed.substr(1);
    }
    if (pressed.toLowerCase() == 'audace') {
        egg = new Tone.Player(audioFolder+'audaceweb.mp3', function() {
            easterEggStart(egg, 10);
        }).toMaster().sync();
    } else if (pressed.substr(1).toLowerCase() == 'qvgdm') {
        egg = new Tone.Player(audioFolder+'qvgdm.mp3', function() {
            easterEggStart(egg, 5);
        }).toMaster().sync();
    }
    return pressed;
}

$(document).ready(function() {
    // Original musics are stored in JSON files.
    let themes = ['kirby', 'mario', 'metroid', 'zelda'];
    let theme = themes[parseInt(getRandomArbitrary(0, themes.length))];
    var bass1 = null;
    var bass2 = null;
    var synth1 = null;
    var synth2 = null;
    var newBass1 = null;
    var newBass2 = null;
    var newSynth1 = null;
    var newSynth2 = null;
    var trackLength = 0;
    // The notes from the chosen theme are shuffled.
    $.getJSON(prestashop.urls.base_url+jsonFolder+theme+'/bass1.json', function(notes) {
        bass1 = shuffleNotes(notes);
        processAfterShuffle(bass1, bass2, synth1, synth2);
    });
    $.getJSON(prestashop.urls.base_url+jsonFolder+theme+'/bass2.json', function(notes) {
        bass2 = shuffleNotes(notes);
        processAfterShuffle(bass1, bass2, synth1, synth2);
    });
    $.getJSON(prestashop.urls.base_url+jsonFolder+theme+'/synth1.json', function(notes) {
        synth1 = shuffleNotes(notes);
        processAfterShuffle(bass1, bass2, synth1, synth2);
    });
    $.getJSON(prestashop.urls.base_url+jsonFolder+theme+'/synth2.json', function(notes) {
        synth2 = shuffleNotes(notes);
        processAfterShuffle(bass1, bass2, synth1, synth2);
    });
    let pressed = ' ';
    $('body').keypress(function(event) {
        pressed = easterEgg(pressed, event.key);
    })
});

{if $elements}
  <div id="block-reassurance">
    <ul>
      {foreach from=$elements item=element}
        <li><span>{$element.text}</span></li>
      {/foreach}
    </ul>
  </div>
{/if}

<div class="banner my-3 text-uppercase font-weight-bold marquee">
  <div class="marquee-item marquee-fire">
    {if isset($banner_img)}
      <img src="{$banner_img}" alt="{$banner_desc}" title="{$banner_desc}" class="img-fluid">
    {else}
      <span class="px-3 py-2 d-block text-center easter" data-easter="0">{$banner_desc}</span>
    {/if}
  </div>
</div>

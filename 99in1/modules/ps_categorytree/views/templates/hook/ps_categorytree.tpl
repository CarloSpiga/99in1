{function name="categories" nodes=[] depth=0}
  {strip}
    {if $nodes|count}
      <ul class="list-styled navbar-nav flex-column">
        {foreach from=$nodes item=node}
          <li class="nav-item">
            {if $depth == 0}<hr>{/if}
            <a href="{$node.link}" class="nav-link{if $depth == 0} text-center text-uppercase font-weight-bold{/if}">{$node.name}</a>
            <div>
              {categories nodes=$node.children depth=$depth+1}
            </div>
          </li>
        {/foreach}
      </ul>
    {/if}
  {/strip}
{/function}

<nav class="category-tree navbar navbar-expand-lg">
  <a class="navbar-brand text-uppercase d-lg-none" href="{$urls.base_url}">{$shop.name}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCategory" aria-controls="navbarCategory" aria-expanded="false" aria-label="{l s='Toggle navigation' d='Modules.Categorytree.Shop'}">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCategory">
    {if $categories.id != 2}
      <ul class="list-styled navbar-nav flex-column">
        <li class="nav-item"><a href="{$categories.link nofilter}" class="nav-link">{$categories.name}</a>{categories nodes=$categories.children}</li>
      </ul>
    {else}
      {categories nodes=$categories.children}
    {/if}
  </div>
</nav>

<address class="block-contact" itemtype="http://schema.org/PostalAddress" itemscope>
  <h4>{l s='Store information' d='Modules.Contactinfo.Shop'}</h4>
  {if $contact_infos.address.address1}
    <br>
    {* First tag [1][/1] is for a HTML tag. *}
    {l s='[1]%address1%[/1]'
      sprintf=[
        '[1]' => '<span itemprop="streetAddress">',
        '[/1]' => '</span>',
        '%address1%' => $contact_infos.address.address1
      ]
      d='Modules.Contactinfo.Shop'
    }
  {/if}
  {if $contact_infos.address.address2}
    <br>
    {* First tag [1][/1] is for a HTML tag. *}
    {l s='[1]%address2%[/1]'
      sprintf=[
        '[1]' => '<span itemprop="streetAddress">',
        '[/1]' => '</span>',
        '%address2%' => $contact_infos.address.address2
      ]
      d='Modules.Contactinfo.Shop'
    }
  {/if}
  {if $contact_infos.address.postcode}
    <br>
    {* First tag [1][/1] is for a HTML tag. *}
    {l s='[1]%postcode%[/1]'
      sprintf=[
        '[1]' => '<span itemprop="postalCode">',
        '[/1]' => '</span>',
        '%postcode%' => $contact_infos.address.postcode
      ]
      d='Modules.Contactinfo.Shop'
    }
  {/if}
  {if $contact_infos.address.city}
    {* First tag [1][/1] is for a HTML tag. *}
    {l s='[1]%city%[/1]'
      sprintf=[
        '[1]' => '<span itemprop="addressLocality">',
        '[/1]' => '</span>',
        '%city%' => $contact_infos.address.city
      ]
      d='Modules.Contactinfo.Shop'
    }
  {/if}
  {if $contact_infos.address.state}
    <br>
    {* First tag [1][/1] is for a HTML tag. *}
    {l s='[1]%state%[/1]'
      sprintf=[
        '[1]' => '<span itemprop="addressLRegion">',
        '[/1]' => '</span>',
        '%state%' => $contact_infos.address.state
      ]
      d='Modules.Contactinfo.Shop'
    }
  {/if}
  {if $contact_infos.address.country}
    <br>
    {* First tag [1][/1] is for a HTML tag. *}
    {l s='[1]%country%[/1]'
      sprintf=[
        '[1]' => '<span itemprop="addressCountry">',
        '[/1]' => '</span>',
        '%country%' => $contact_infos.address.country
      ]
      d='Modules.Contactinfo.Shop'
    }
  {/if}
  {if $contact_infos.phone}
    <br>
    {* First tag [1][/1] is for a HTML tag. *}
    {l s='Call us: [1]%phone%[/1]'
      sprintf=[
        '[1]' => '<span itemprop="telephone">',
        '[/1]' => '</span>',
        '%phone%' => $contact_infos.phone
      ]
      d='Modules.Contactinfo.Shop'
    }
  {/if}
  {if $contact_infos.fax}
    <br>
    {* First tag [1][/1] is for a HTML tag. *}
    {l
      s='Fax: [1]%fax%[/1]'
      sprintf=[
        '[1]' => '<span itemprop="faxNumber">',
        '[/1]' => '</span>',
        '%fax%' => $contact_infos.fax
      ]
      d='Modules.Contactinfo.Shop'
    }
  {/if}
  {if $contact_infos.email}
    <br>
    {* First tag [1][/1] is for a HTML tag. *}
    {l
      s='Email us: [1]%email%[/1]'
      sprintf=[
        '[1]' => '<span itemprop="email">',
        '[/1]' => '</span>',
        '%email%' => $contact_infos.email
      ]
      d='Modules.Contactinfo.Shop'
    }
  {/if}
</address>

<section>
  <h3 class="text-center text-uppercase text-secondary">{l s='Our Products' d='Modules.Featuredproducts.Shop'}</h3>
  <div class="products">
    {foreach from=$products item="product"}
      {include file="catalog/_partials/miniatures/product.tpl" product=$product}
    {/foreach}
  </div>
  <a href="{$allProductsLink}" class="d-block text-center text-uppercase font-size-lg">{l s='All products' d='Modules.Featuredproducts.Shop'}</a>
</section>

{if $homeslider.slides}
  <div class="homeslider-container" data-interval="{$homeslider.speed}" data-wrap="{$homeslider.wrap}" data-pause="{$homeslider.pause}">
    <ul class="rslides">
      {foreach from=$homeslider.slides item=slide}
        <li class="slide">
          <a href="{$slide.url}" class="text-center">
            <img src="{$slide.image_url}" alt="{$slide.legend|escape}" class="img-fluid">
            {if $slide.title || $slide.description }
              <div class="caption">
                {if $slide.title}<h2>{$slide.title}</h2>{/if}
                {if $slide.description}{$slide.description nofilter}{/if}
              </div>
            {/if}
          </a>
        </li>
      {/foreach}
    </ul>
  </div>
{/if}

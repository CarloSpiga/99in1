<div class="row justify-content-around">
  {foreach $linkBlocks as $linkBlock}
    <div class="col-auto">
      <h3 class="text-secondary">{$linkBlock.title}</h3>
      <ul class="list-unstyled">
        {foreach $linkBlock.links as $link}
          <li>
            <a
              id="{$link.id}-{$linkBlock.id}"
              class="{$link.class}"
              href="{$link.url}"
              title="{$link.description}"
              {if !empty($link.target)} target="{$link.target}" {/if}
            >
              {$link.title}
            </a>
          </li>
        {/foreach}
      </ul>
    </div>
  {/foreach}
</div>

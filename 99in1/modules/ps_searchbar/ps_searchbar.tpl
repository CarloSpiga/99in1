<div id="search_widget" data-search-controller-url="{$search_controller_url}">
	<form method="get" action="{$search_controller_url}" class="form-row">
		<input type="hidden" name="controller" value="search">
    <div class="col-auto">
      <div class="input-group">
        <input type="text" name="s" value="{$search_string}" class="form-control">
        <div class="input-group-append">
      		<button class="btn-primary" type="submit">
      			{l s='Search' d='Modules.Searchbar.Shop'}
      		</button>
        </div>
      </div>
    </div>
	</form>
</div>

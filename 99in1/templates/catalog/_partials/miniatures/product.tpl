{block name='product_miniature_item'}
  <article class="product-miniature row" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">

    <div class="col-lg-4">
      {block name='product_thumbnail'}
        <a href="{$product.url}" class="thumbnail product-thumbnail">
          <img
            src = "{$product.cover.medium.url}"
            alt = "{$product.cover.legend}"
            data-full-size-image-url = "{$product.cover.large.url}"
          >
        </a>
      {/block}
    </div>

    <div class="col-lg-8">
      {block name='product_name'}
        <h2 class="h2" itemprop="name"><a href="{$product.url}">{$product.name}</a></h2>
      {/block}

      {block name='product_description_short'}
        <div class="product-description-short text-justify" itemprop="description">{$product.description_short nofilter}</div>
      {/block}

      {block name='product_list_actions'}
        <div class="product-list-actions">
          {if $product.add_to_cart_url}
              <a
                class = "add-to-cart btn btn-primary mb-2"
                href  = "{$product.add_to_cart_url}"
                rel   = "nofollow"
                data-id-product="{$product.id_product}"
                data-id-product-attribute="{$product.id_product_attribute}"
                data-link-action="add-to-cart"
              >{l s='Add to cart' d='Shop.Theme.Actions'}</a>
          {/if}
          {hook h='displayProductListFunctionalButtons' product=$product}
        </div>
      {/block}

      {block name='product_quick_view'}
      {/block}

      {block name='product_variants'}
        {include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
      {/block}

      {block name='product_price_and_shipping'}
        {if $product.show_price}
          <div class="product-price-and-shipping label-meto">
            {if $product.has_discount}
              {hook h='displayProductPriceBlock' product=$product type="old_price"}

              <span class="regular-price line-through text-primary font-size-sm">{$product.regular_price}</span>
              {if $product.discount_type === 'percentage'}
                <span class="discount-percentage discount-product text-primary">{$product.discount_percentage}</span>
              {elseif $product.discount_type === 'amount'}
                <span class="discount-amount discount-product text-primary">{$product.discount_amount_to_display}</span>
              {/if}
            {/if}

            {hook h='displayProductPriceBlock' product=$product type="before_price"}

            <span itemprop="price" class="price">{$product.price}</span>

            {hook h='displayProductPriceBlock' product=$product type="unit_price"}

            {hook h='displayProductPriceBlock' product=$product type="weight"}
          </div>
        {/if}
      {/block}

      {block name='product_flags'}
        <ul class="product-flags list-unstyled mt-2">
          {foreach from=$product.flags item=flag}
            <li class="{$flag.type} blink text-secondary">{$flag.label}</li>
          {/foreach}
        </ul>
      {/block}

      {block name='product_availability'}
        {if $product.show_availability}
          {* availability may take the values "available" or "unavailable" *}
          <span class='product-availability {$product.availability}'>{$product.availability_message}</span>
        {/if}
      {/block}
    </div>

    {hook h='displayProductListReviews' product=$product}

  </article>
{/block}

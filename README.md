# 99in1 Prestashop 1.7 theme and modules

Prestashop 1.7 theme with retrogaming and retrowebing influences, optional customized modules are included.
A design from 1999 made with 2019 technologies.

## Installation

- Make sure that the `upload_max_filesize` value of your `php.ini` configuration file is set at least to `8M` if you wish to import the theme as a .zip archive.
- Make sure that the .zip archive with the theme contains every file and folder from the `99in1` folder except `_dev`, and that the files and folders are directly placed at the root of the archive.

## Creation context

This theme was developed the for 99 in 1 company, which sells counterfeit video game cartridges.
In the late 90s, those cartridges were quite popular amongst video games lovers who didn't have enough money to pay high-end video game consoles. So alternative consoles (also named Famiclones) emerged with those low-cost cartridges that could contain dozens of games from the 80s. Their names often came from the amount of games that were playable, which was an **arbitrary amount of nines, playable in one cartridge**. In reality, there were only about twenty different games, repeated an arbitrary amount of times in a list stored in the cartridge. You could then select a game through the interface and play with it.

Today, retrogaming is a trending topic for a whole generation that grew up with video games from the 80s or the 90s, but Famiclones almost disappeared with time. The goal of the 99 in 1 company is to revive this side of the video game history, and to recreate famiclones and their cartridges, sold at low prices to make them accessible for everyone. Their website will sell consoles, but also customized cartridges. Customers will be able to put games in their carts, then the company will create a cartridge with all games listed in the order, and send it to the customer.

## Theme influences

Based on gamer's nostalgia, the theme is mostly inspired by old video games graphics (arcade, and home consoles from the 80s). Like almost all websites layouts nowadays, the theme uses [Bootstrap](https://getbootstrap.com/) and [jQuery](https://jquery.com/) with semantic HTML and CSS, and responsive layout. But the theme will try to recreate old websites templates (with containers that look like tables, background images but compressed, more than two colors but without tributing ergonomics, ...), like the ones that were popular before the smartphone era (like [Absoluflash](https://web.archive.org/web/20130802082247/http://www.absoluflash.com/), [1980 Games](http://www.1980-games.com/), [Space Jam](https://www.spacejam.com/archive/spacejam/movie/jam.htm) and [Consoles Pix](http://3615pixels.cool/)).

## Optional modules

To follow company's specifications, the following modules are included with the theme, but you can disable them in your back-office :
- [99in1 Emulation](99in1/dependencies/modules/nnio_emulation/README.md) : emulators integration on product pages.
- [99in1 Random 8-bit Music](99in1/dependencies/modules/nnio_random8bitmusic/README.md) : random music played on pages.
- [Audaceweb Customers Counter](99in1/dependencies/modules/adcwb_customerscounter/README.md) : shows a customer counter in the footer.
- [Audaceweb Remove Default Assets](99in1/dependencies/modules/adcwb_removedefaultassets/README.md) : switch between logo and banner images and CSS styles by enabling/disabling the module.
- [Audaceweb SEO](99in1/dependencies/modules/adcwb_seo/README.md) : adds keywords on the page for old-style SEO.

## Development environment

### Text editor

[Atom](https://atom.io/) is used as the the text editor, with the following packages :
- [editorconfig](https://atom.io/packages/editorconfig) for global file linting.
- [emmet](https://atom.io/packages/emmet) for the HTML generation.
- [file-icons](https://atom.io/packages/file-icons) to see file types in the tree view more efficiently.
- [linter-eslint](https://atom.io/packages/linter-eslint) to lint JavaScript files, following the Prestashop recommended [`.eslintrc.js` file](https://github.com/PrestaShop/PrestaShop/blob/develop/.eslintrc.js).
- [php-cs-fixer](https://atom.io/packages/php-cs-fixer) to lint PHP files, following the PSR-2 coding style guide.
- [platforio-ide-terminal](https://atom.io/packages/platformio-ide-terminal) to have an integrated console with webpack watch running.
- [remote-ftp](https://atom.io/packages/remote-ftp) to sync local environment with remote server.

### Configuration

- The theme has the [StarterTheme](https://github.com/PrestaShop/StarterTheme) Prestashop theme as a base, deprecated three days before the 99 in 1 theme was finished.
- `package.json` and `webpack.config.json` were updated to accept updated versions of modules used.
- PostCSS, LESS, Stylus and Internet Explorer compatibility have been dropped.
- Bootstrap 4.3.1 has been added.
- [VCR OSD Mono](https://www.dafont.com/fr/vcr-osd-mono.font) font used.
- A `workspace` folder is included with analysis documents, like mockups and hook list.
- All assets were processed through the `_dev` folder, with SASS and JavaScript ES6 classes.

# ROADMAP

- Put JavaScript optimization and translations on modules.
- Test Audaceweb Remove Default Assets module with other modules overriding the FrontController and the ps_banner.
- Test Audaceweb SEO with multiple langs and multiple shops.

# When development ends

- Clean server and database.
- Send a mail to olivier@prestatill.com with clear object with name and surname, with admin account, FTP and database access, odt.
